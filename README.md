# ! README ! #

- Ini adalah tempat penyimpanan data-data mining, program modfikasi mining, daftar mekanisme proyek mining, dll

# ! DAFTAR LINK ! #

- Link Program XMRIG "Indonesia" ( Versi 6.11.1 ) dengan donasi 0% : none
- Link Program XMRIG "English" ( Versi 6.11.1 ) dengan donasi 0% : https://bitbucket.org/anli-projects/storage/downloads/masterfile

# ! :) FOLLOW YA :) ! #

- Twitter : https://twitter.com/Anli_Notes

# ! CREDIT ! #

- Terima Kasih : Pengembang XMRIG ( https://github.com/xmrig/xmrig )

# ! I LOVE YOU ! #

- Kalau mau donasi Monero ke Saya. Berapa aja. Terima Kasih. : 46LZKT4LHCENcU9GtWtCe4NuLSrxtPpHKJs5J5xLqoE8EMKWmc142s2AQvn64n3E5vVbfSYD2KbenUz8CzjMv2trSrwB24v

# ! BUILD COMMAND ! #
Platform yang saya gunakan untuk mem-build kali ini adalah dengan menggunakan Goorm. Berikut command line :

- sudo apt-get upgrade
- sudo apt-get install git build-essential cmake automake libtool autoconf
- git clone your-xmrig-link-here
- mkdir xmrig/build && cd xmrig/scripts
- ./build_deps.sh && cd ../build
- cmake .. -DXMRIG_DEPS=scripts/deps
- make -j$(nproc)
- *Setelah tahapan ini ubah nama file menjadi "masterfile"
- ldd masterfile ( command ini untuk memverifikasi biner file dari masterfile yang sudah di build )

# ! THANK YOU ~ ANLI ANGKU ! #